#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <android_native_app_glue.h>
#include <time.h>
#include "glesutil.h"
#include "defs.h"
extern const char *vertexshader,*fragmentshader;
void init_display(struct global *global){
	global->running=true;
	srand48(time(NULL));
	initextensions();
	global->windoww=ANativeWindow_getWidth(global->app->window);
	global->windowh=ANativeWindow_getHeight(global->app->window);
	global->display=eglGetDisplay(EGL_DEFAULT_DISPLAY);
	eglInitialize(global->display,NULL,NULL);
	EGLConfig config;
	int configcount;
	eglChooseConfig(global->display,(int[]){EGL_RED_SIZE,8,EGL_GREEN_SIZE,8,EGL_BLUE_SIZE,8,EGL_NONE},&config,1,&configcount);
	ANativeWindow_setBuffersGeometry(global->app->window,960,540,0);
	global->surface=eglCreateWindowSurface(global->display,config,global->app->window,NULL);
	global->context=eglCreateContext(global->display,config,NULL,(int[]){EGL_CONTEXT_CLIENT_VERSION,2,EGL_NONE});
	eglMakeCurrent(global->display,global->surface,global->surface,global->context);
	if(!loadpack(&global->assets,global->app->activity->assetManager,"assets.pack",FILTER_ASSETS))log("texture init error");
	if(!loadpack(&global->uiassets,global->app->activity->assetManager,"uiassets.pack",NULL))log("texture init error");
	global->program=initshaders(vertexshader,fragmentshader);
	glUseProgram(global->program);
	global->uniform.vector=glGetUniformLocation(global->program,"vector");
	global->uniform.size=glGetUniformLocation(global->program,"size");
	global->uniform.texcoords=glGetUniformLocation(global->program,"texcoords");
	global->uniform.rgba=glGetUniformLocation(global->program,"rgba");
	global->uniform.rot=glGetUniformLocation(global->program,"rot");
	global->uniform.projection=glGetUniformLocation(global->program,"projection");
	float matrix[16];
	initortho(matrix,global->rect.left,global->rect.right,global->rect.bottom,global->rect.top,-1.0f,1.0f);
	glUniformMatrix4fv(global->uniform.projection,1,false,matrix);
	glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
	glGenBuffers(1,&global->vbo);
	glGenVertexArrays(1,&global->vao);
	glBindBuffer(GL_ARRAY_BUFFER,global->vbo);
	glBindVertexArray(global->vao);
	const float verts[]={-0.5f,-0.5f,-0.5f,0.5f,0.5f,-0.5f,0.5f,0.5f};
	glBufferData(GL_ARRAY_BUFFER,sizeof(float)*8,verts,GL_STATIC_DRAW);
	glVertexAttribPointer(0,2,GL_FLOAT,false,0,0);
	glEnableVertexAttribArray(0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	set_ftfont_params(960,540,16.0f,9.0f,global->uniform.vector,global->uniform.size,global->uniform.texcoords);
	global->font1=create_ftfont(global->app->activity->assetManager,0.5f,FONT_DISPLAY);
	global->font2=create_ftfont(global->app->activity->assetManager,0.7f,FONT_MAIN);
	loadapack(&global->sassets,global->app->activity->assetManager,"sassets.spack");
	global->soundengine=initOpenSL();
	playsound(global->soundengine,&global->sassets.sound[SID_MUSIC],true);
}
void term_display(struct global *global){
	global->running=false;
	destroy_ftfont(global->font1);
	destroy_ftfont(global->font2);
	destroypack(&global->assets);
	destroypack(&global->uiassets);
	glDeleteBuffers(1,&global->vbo);
	glDeleteVertexArrays(1,&global->vao);
	glDeleteProgram(global->program);
	eglMakeCurrent(global->display,EGL_NO_SURFACE,EGL_NO_SURFACE,EGL_NO_CONTEXT);
	eglDestroyContext(global->display,global->context);
	eglDestroySurface(global->display,global->surface);
	eglTerminate(global->display);
	termOpenSL(global->soundengine);
	destroyapack(&global->sassets);
}
int32_t inputproc(struct android_app *app,AInputEvent *event){
	struct global *global=app->userData;
	int32_t type=AInputEvent_getType(event);
	if(type==AINPUT_EVENT_TYPE_MOTION){
		int32_t action=AMotionEvent_getAction(event);
		int pointerid=AMotionEvent_getPointerId(event,GET_POINTER_INDEX(action));
		if(pointerid>1)return true;
		switch(action&AMOTION_EVENT_ACTION_MASK){
			case AMOTION_EVENT_ACTION_DOWN:
			case AMOTION_EVENT_ACTION_POINTER_DOWN:
				global->pointer[pointerid].active=true;
			case AMOTION_EVENT_ACTION_MOVE:
				for(int i=0,pointercount=AMotionEvent_getPointerCount(event);i<pointercount;i++){
					pointerid=AMotionEvent_getPointerId(event,i);
					if(pointerid>1)return true;
					global->pointer[pointerid].x=((AMotionEvent_getX(event,i)/global->windoww)*global->rect.right*2.0f)-global->rect.right;
					global->pointer[pointerid].y=((AMotionEvent_getY(event,i)/global->windowh)*global->rect.bottom*2.0f)-global->rect.bottom;
				}
				break;
			case AMOTION_EVENT_ACTION_UP:
			case AMOTION_EVENT_ACTION_POINTER_UP:
				global->pointer[pointerid].active=false;
				break;
			default: return false; break;
		}
		return true;
	}
	else if(type==AINPUT_EVENT_TYPE_KEY){
		int32_t key=AKeyEvent_getKeyCode(event);
		switch(key){
			case AKEYCODE_SEARCH:
				if(AKeyEvent_getAction(event)==AKEY_EVENT_ACTION_DOWN)global->search=true;
				break;
			case AKEYCODE_BACK:
				if(AKeyEvent_getAction(event)==AKEY_EVENT_ACTION_DOWN)global->back=true;
				break;
			default:
				return false;
		}
		return true;
	}
	return false;
}
void cmdproc(struct android_app *app,int32_t cmd){
	struct global *global=app->userData;
	switch(cmd){
		case APP_CMD_DESTROY:
			log("APP_CMD_DESTROY");
			reset(app->userData,false);
			break;
		case APP_CMD_INIT_WINDOW:
			init_display(app->userData);
			break;
		case APP_CMD_TERM_WINDOW:
			term_display(app->userData);
			break;
	}
}
int process(struct android_app *app){
	int ident,events;
	struct android_poll_source *source;
	while(ident=ALooper_pollAll(((struct global*)app->userData)->running?0:-1,NULL,&events,(void**)&source)>=0){
		if(source)source->process(app,source);
		if(app->destroyRequested){
			return false;
		}
	}
	return true;
}
void android_main(struct android_app *app){
	log("--- BEGIN NEW LOG ---");
	app_dummy();
	struct global global;
	global.app=app;
	global.running=false;
	app->userData=&global;
	app->onAppCmd=cmdproc;
	app->onInputEvent=inputproc;
	init(&global);
	reset(&global,true);
	while(process(app)&&core(&global)){
		render(&global);
		eglSwapBuffers(global.display,global.surface);
	}
	log("exit");
}
