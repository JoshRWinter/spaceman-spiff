#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <android_native_app_glue.h>
#include <math.h>
#include "glesutil.h"
#include "defs.h"
int core(struct global *global){
	if(global->showmenu){
		global->showmenu=false;
		if(!menu_main(global))return false;
		global->search=false;
	}
	if(global->search){
		menu_message(global,"Don't push that.");
		global->search=false;
	}
	if(touching(global,&global->firebutton,&global->firebutton.activepointer)){
		if(!global->player.reload){
			newplaser(global);
			playsound(global->soundengine,&global->sassets.sound[SID_PLASER],false);
			global->player.reload=10;
		}
	}
	else global->firebutton.activepointer=-1;
	if(global->player.reload)global->player.reload--;
	
	if(touching(global,&(struct generic){0,global->rect.left,global->rect.top,global->rect.right,global->rect.bottom*2.0f},&global->joystick_top.activepointer)){
		global->joystick_top.x=global->pointer[global->joystick_top.activepointer].x-(JOYSTICK_BASE_SIZE/2.0f);
		global->joystick_top.y=global->pointer[global->joystick_top.activepointer].y-(JOYSTICK_BASE_SIZE/2.0f);
		float throttle=dist(global->joystick_top.x,global->joystick_top.y,JOYSTICK_BASE_X,JOYSTICK_BASE_Y);
		float angle=atan2f(JOYSTICK_BASE_Y+(JOYSTICK_BASE_SIZE/2.0f)-global->pointer[global->joystick_top.activepointer].y,
		JOYSTICK_BASE_X+(JOYSTICK_BASE_SIZE/2.0f)-global->pointer[global->joystick_top.activepointer].x);
		if(throttle>JOYSTICK_DIST){
			throttle=JOYSTICK_DIST;
			global->joystick_top.x=JOYSTICK_BASE_X-cosf(angle)*JOYSTICK_DIST;
			global->joystick_top.y=JOYSTICK_BASE_Y-sinf(angle)*JOYSTICK_DIST;
		}
		const float speed=18.0f;
		global->player.xup=-cos(angle)*throttle/speed;
		global->player.yup=-sin(angle)*throttle/speed;
		global->player.x+=global->player.xup;
		global->player.y+=global->player.yup;
		global->player.rot=-angle;
		if(randomint(0,1)==1)newsmoke(global,global->player.x+(PLAYER_WIDTH/2.0f),global->player.y+(PLAYER_HEIGHT/2.0f),SMOKE_EXHAUST);
	}
	else{
		global->joystick_top.x=JOYSTICK_BASE_X;
		global->joystick_top.y=JOYSTICK_BASE_Y;
		global->player.xup=0.0f;
		global->player.yup=0.0f;
	}
	if(global->player.x<global->world.left){
		global->player.x=global->world.left;
		global->player.xup=0.0f;
	}
	else if(global->player.x+PLAYER_WIDTH>global->world.right){
		global->player.x=global->world.right-PLAYER_WIDTH;
		global->player.xup=0.0f;
	}
	if(global->player.y<global->world.top){
		global->player.y=global->world.top;
		global->player.yup=0.0f;
	}
	else if(global->player.y+PLAYER_HEIGHT>global->world.bottom){
		global->player.y=global->world.bottom-PLAYER_HEIGHT;
		global->player.yup=0.0f;
	}
	if(global->player.stun)global->player.stun--;
	targetf(&global->player.displayhealth,(fabs(global->player.health-global->player.displayhealth)/8.0f)+0.01f,global->player.health);
	if(global->player.health<1){
		return menu_gameover(global);
	}
	if(global->spawnerlist==NULL){
		return menu_gameover(global);
	}
	
	for(struct enemy *enemy=global->enemylist,*prevenemy=NULL;enemy!=NULL;){
		if(enemy->stun)enemy->stun--;
		if(enemy->reload)enemy->reload--;
		
		if(enemy->mode==ENEMY_MODE_DISABLE){
			enemy->rot+=enemy->xtarget;
			enemy->x+=enemy->xup;
			enemy->y+=enemy->yup;
		}
		else if(enemy->mode==ENEMY_MODE_PROTECT){
			if(onein(2))newsmoke(global,enemy->x+(ENEMY_WIDTH/2.0f),enemy->y+(ENEMY_HEIGHT/2.0f),SMOKE_ENEMY_EXHAUST);
			enemy->rot=-atan2f((enemy->y+(ENEMY_HEIGHT/2.0f))-(enemy->spawner->y+(SPAWNERCORE_HEIGHT/2.0f)),(enemy->x+(ENEMY_WIDTH/2.0f))-(enemy->spawner->x+(SPAWNERCORE_WIDTH/2.0f)));
			enemy->xup=-cosf(-enemy->rot)/15.0f;
			enemy->yup=-sinf(-enemy->rot)/15.0f;
			enemy->x+=enemy->xup;
			enemy->y+=enemy->yup;
			if(collide(enemy,&(struct generic){0,enemy->spawner->x+(SPAWNERCORE_WIDTH/2.0f),enemy->spawner->y+(SPAWNERCORE_HEIGHT/2.0f),0,0})){
				enemy->spawner->carrier=enemy;
				for(struct enemy *enemy2=global->enemylist;enemy2!=NULL;enemy2=enemy2->next){
					if(enemy2->spawner==enemy->spawner)enemy2->mode=ENEMY_MODE_FIRE;
				}
			}
		}
		else if(enemy->mode==ENEMY_MODE_FIRE){
			enemy->rottarget=-atan2f((enemy->y+(ENEMY_HEIGHT/2.0f))-(global->player.y+(PLAYER_HEIGHT/2.0f)),(enemy->x+(ENEMY_WIDTH/2.0f))-(global->player.x+(PLAYER_WIDTH/2.0f)));
			enemypoint(enemy);
			if(fabs(enemy->rot-enemy->rottarget)<0.01f){
				if(onein(25)&&!enemy->reload){
					newelaser(global,enemy);
					playsound(global->soundengine,&global->sassets.sound[SID_ELASER],false);
					enemy->reload=ENEMY_RELOAD;
				}
			}
			if(onein(120)){
				enemy->mode=ENEMY_MODE_RELOCATE;
				enemy->xtarget=randomint((enemy->spawner->x+(SPAWNER_WIDTH/2.0f)-ENEMY_SEEK_RANGE)*10.0f,(enemy->spawner->x+(SPAWNER_WIDTH/2.0f)+ENEMY_SEEK_RANGE)*10.0f)/10.0f;
				enemy->ytarget=randomint((enemy->spawner->y+(SPAWNER_HEIGHT/2.0f)-ENEMY_SEEK_RANGE)*10.0f,(enemy->spawner->y+(SPAWNER_HEIGHT/2.0f)+ENEMY_SEEK_RANGE)*10.0f)/10.0f;
				enemy->rottarget=-atan2f(enemy->y+(ENEMY_HEIGHT/2.0f)-enemy->ytarget,enemy->x+(ENEMY_WIDTH/2.0f)-enemy->xtarget);
			}
		}
		else if((enemy->mode==ENEMY_MODE_IDLE&&onein(220))||(collide(enemy,enemy->spawner)&&enemy->mode!=ENEMY_MODE_RELOCATE)){
			enemy->mode=ENEMY_MODE_RELOCATE;
			enemy->xtarget=randomint((enemy->spawner->x+(SPAWNER_WIDTH/2.0f)-ENEMY_SEEK_RANGE)*10.0f,(enemy->spawner->x+(SPAWNER_WIDTH/2.0f)+ENEMY_SEEK_RANGE)*10.0f)/10.0f;
			enemy->ytarget=randomint((enemy->spawner->y+(SPAWNER_HEIGHT/2.0f)-ENEMY_SEEK_RANGE)*10.0f,(enemy->spawner->y+(SPAWNER_HEIGHT/2.0f)+ENEMY_SEEK_RANGE)*10.0f)/10.0f;
			enemy->rottarget=-atan2f(enemy->y+(ENEMY_HEIGHT/2.0f)-enemy->ytarget,enemy->x+(ENEMY_WIDTH/2.0f)-enemy->xtarget);
		}
		else if(enemy->mode==ENEMY_MODE_RELOCATE){
			if(onein(2))newsmoke(global,enemy->x+(ENEMY_WIDTH/2.0f),enemy->y+(ENEMY_HEIGHT/2.0f),SMOKE_ENEMY_EXHAUST);
			if(enemypoint(enemy)){
				const float speed=enemy->spawner->alert?(randomint(100,175)/10.0f):(randomint(100,250)/10.0f);
				enemy->xup=-cosf(-enemy->rot)/speed;
				enemy->yup=-sinf(-enemy->rot)/speed;
				enemy->x+=enemy->xup;
				enemy->y+=enemy->yup;
			}
			if(collide(enemy,&(struct generic){0,enemy->xtarget,enemy->ytarget,0.0f,0.0f})){
				enemy->xup=0.0f;
				enemy->yup=0.0f;
				enemy->mode=enemy->spawner->alert?ENEMY_MODE_FIRE:ENEMY_MODE_IDLE;
			}
		}
		prevenemy=enemy;
		enemy=enemy->next;
	}
	
	for(struct spawner *spawner=global->spawnerlist,*prevspawner=NULL;spawner!=NULL;){
		if(spawner->carrier){
			spawner->x=spawner->carrier->x+(ENEMY_WIDTH/2.0f)-(SPAWNERCORE_WIDTH/2.0f);
			spawner->y=spawner->carrier->y+(ENEMY_HEIGHT/2.0f)-(SPAWNERCORE_HEIGHT/2.0f);
		}
		else{
			spawner->x+=spawner->xup;
			spawner->y+=spawner->yup;
		}
		if(spawner->x+SPAWNERCORE_WIDTH>global->world.right)spawner->x=global->world.right-SPAWNERCORE_WIDTH;
		else if(spawner->x<global->world.left)spawner->x=global->world.left;
		if(spawner->y+SPAWNERCORE_HEIGHT>global->world.bottom)spawner->y=global->world.bottom-SPAWNERCORE_HEIGHT;
		else if(spawner->y<global->world.top)spawner->y=global->world.top;
		spawner->rot+=spawner->rotup;
		if(spawner->rot>PI2)spawner->rot=0.0f;
		else if(spawner->rot<0.0f)spawner->rot=PI2;
		if((spawner->guardcount<SPAWNER_MAX_GUARD_COUNT&&onein(200)&&spawner->health>0)||(spawner->guardcount==0&&spawner->health>0)){
			spawner->guardcount++;
			newenemy(global,spawner);
		}
		if(spawner->carrier==NULL&&spawner->health<1&&collide(&(struct generic){0,spawner->x+(SPAWNERCORE_WIDTH/2.0f),spawner->y+(SPAWNERCORE_HEIGHT/2.0f),0,0},&global->player)){
			for(struct enemy *enemy=global->enemylist;enemy!=NULL;enemy=enemy->next){
				if(spawner==enemy->spawner){
					enemy->mode=ENEMY_MODE_DISABLE;
					enemy->spawner=NULL;
					enemy->xup/=4.0f;
					enemy->yup/=4.0f;
					enemy->xtarget=randomint(-9,9)/1000.0f;// doubles as rotup
				}
			}
			spawner=deletespawner(global,spawner,prevspawner);
			global->messagetext=MESSAGE_SPAWNER;
			global->messagetimer=MESSAGE_TIMER;
			continue;
		}
		else if(spawner->health>0&&collide(spawner,&global->player)){
			global->player.stun=100;
			global->player.health-=2;
		}
		if(inrange(spawner)&&spawner->health>0){
			if(!spawner->alert){
				for(struct enemy *enemy=global->enemylist;enemy!=NULL;enemy=enemy->next){
					if(enemy->spawner==spawner)enemy->mode=ENEMY_MODE_FIRE;
				}
			}
			spawner->alert=true;
		}
		else if(spawner->alert&&spawner->health>0){
			spawner->alert++;
			if(spawner->alert>ENEMY_FORGET){
				spawner->alert=0;
				for(struct enemy *enemy=global->enemylist;enemy!=NULL;enemy=enemy->next){
					if(enemy->spawner==spawner)enemy->mode=ENEMY_MODE_IDLE;
				}
			}
		}
		prevspawner=spawner;
		spawner=spawner->next;
	}
	
	for(struct elaser *elaser=global->elaserlist,*prevelaser=NULL;elaser!=NULL;){
		elaser->x+=elaser->xup;
		elaser->y+=elaser->yup;
		const float ACCEL=1.075f;
		elaser->xup*=ACCEL;
		elaser->yup*=ACCEL;
		if(collide(elaser,&global->player)){
			global->player.stun=45;
			elaser=deleteelaser(global,elaser,prevelaser);
			global->player.health-=randomint(3,5);
			continue;
		}
		if(!elaser->ttl--){
			newsmoke(global,elaser->x+(ELASER_WIDTH/2.0f),elaser->y+(ELASER_HEIGHT/2.0f),SMOKE_ENEMY_DESTROY);
			elaser=deleteelaser(global,elaser,prevelaser);
			continue;
		}
		prevelaser=elaser;
		elaser=elaser->next;
	}
	
	for(struct plaser *plaser=global->plaserlist,*prevplaser=NULL;plaser!=NULL;){
		plaser->x+=plaser->xup;
		plaser->y+=plaser->yup;
		const float ACCEL=1.085f;
		plaser->xup*=ACCEL;
		plaser->yup*=ACCEL;
		int skip=false;
		for(struct enemy *enemy=global->enemylist,*prevenemy=NULL;enemy!=NULL;){
			if(collide(plaser,enemy)){
				if(enemy->spawner&&enemy->spawner->health>0){
					if(!enemy->spawner->alert){
						for(struct enemy *enemy2=global->enemylist;enemy2!=NULL;enemy2=enemy2->next){
							if(enemy2->spawner==enemy->spawner)enemy2->mode=ENEMY_MODE_FIRE;
						}
					}
					enemy->spawner->alert=true;
				}
				newparticles(global,plaser->x+(PLASER_WIDTH/2.0f),plaser->y+(PLASER_HEIGHT/2.0f),plaser->rot,randomint(5,9));
				enemy->health-=randomint(10,35);
				enemy->stun=ENEMY_STUN;
				if(enemy->health<1){
					newdebris(global,enemy->x+(ENEMY_WIDTH/2.0f),enemy->y+(ENEMY_HEIGHT/2.0f),DEBRIS_ENEMY_WING);
					newdebris(global,enemy->x+(ENEMY_WIDTH/2.0f),enemy->y+(ENEMY_HEIGHT/2.0f),DEBRIS_ENEMY_WING);
					newdebris(global,enemy->x+(ENEMY_WIDTH/2.0f),enemy->y+(ENEMY_HEIGHT/2.0f),DEBRIS_ENEMY_FUSELAGE);
					newhealth(global,enemy->x+(ENEMY_WIDTH/2.0f),enemy->y+(ENEMY_HEIGHT/2.0f));
					newparticles(global,plaser->x+(PLASER_WIDTH/2.0f),plaser->y+(PLASER_HEIGHT/2.0f),plaser->rot,randomint(15,20));
					for(int i=0;i<4;i++)newsmoke(global,enemy->x+(ENEMY_WIDTH/2.0f),enemy->y+(ENEMY_HEIGHT/2.0f),SMOKE_ENEMY_DESTROY);
					if(enemy->spawner!=NULL){
						enemy->spawner->guardcount--;
						if(enemy->spawner->carrier==enemy){
							enemy->spawner->carrier=NULL;
							for(struct enemy *enemy2=global->enemylist;enemy2!=NULL;enemy2=enemy2->next){
								if(enemy->spawner==enemy2->spawner)enemy2->mode=ENEMY_MODE_PROTECT;
							}
						}
					}
					playsound(global->soundengine,&global->sassets.sound[SID_EXPLOSION],false);
					deleteenemy(global,enemy,prevenemy);
				}
				plaser=deleteplaser(global,plaser,prevplaser);
				skip=true;
				break;
			}
			prevenemy=enemy;
			enemy=enemy->next;
		}
		if(skip)continue;
		for(struct spawner *spawner=global->spawnerlist;spawner!=NULL;spawner=spawner->next){
			const float shrink=1.8f;
			if(collide(plaser,&(struct generic){0,spawner->x+(shrink/2.0f),spawner->y+(shrink/2.0f),spawner->w-shrink,spawner->h-shrink})){
				newparticles(global,plaser->x+(PLASER_WIDTH/2.0f),plaser->y+(PLASER_HEIGHT/2.0f),plaser->rot,randomint(5,9));
				newsmoke(global,plaser->x+(PLASER_WIDTH/2.0f),plaser->y+(PLASER_HEIGHT/2.0f),SMOKE_LASER_FIRE);
				if(spawner->health>0){
					if(!spawner->alert){
						for(struct enemy *enemy=global->enemylist;enemy!=NULL;enemy=enemy->next){
							if(spawner==enemy->spawner)enemy->mode=ENEMY_MODE_FIRE;
						}
					}
					spawner->alert=true;
					spawner->health-=randomint(1,2);
					if(spawner->health<1){
						for(struct enemy *enemy=global->enemylist;enemy!=NULL;enemy=enemy->next){
							if(enemy->spawner==spawner)enemy->mode=ENEMY_MODE_PROTECT;
						}
						playsound(global->soundengine,&global->sassets.sound[SID_BIGEXPLOSION],false);
						newparticles(global,spawner->x+(SPAWNER_WIDTH/2.0f),spawner->y+(SPAWNER_HEIGHT/2.0f),plaser->rot,100);
						newdebris(global,spawner->x+(SPAWNER_WIDTH/2.0f),spawner->y+(SPAWNER_HEIGHT/2.0f),DEBRIS_SPAWNER_WING);
						newdebris(global,spawner->x+(SPAWNER_WIDTH/2.0f),spawner->y+(SPAWNER_HEIGHT/2.0f),DEBRIS_SPAWNER_WING);
						spawner->texture=TID_SPAWNERCORE;
						spawner->w=SPAWNERCORE_WIDTH;
						spawner->h=SPAWNERCORE_HEIGHT;
						spawner->x+=SPAWNERCORE_WIDTH/2.0f;
						spawner->y+=SPAWNERCORE_HEIGHT/2.0f;
						spawner->rotup/=1.5f;
						spawner->xup=-cosf(-plaser->rot)/90.0f;
						spawner->yup=-sinf(-plaser->rot)/90.0f;
					}
				}
				plaser=deleteplaser(global,plaser,prevplaser);
				skip=true;
				break;
			}
		}
		if(skip)continue;
		if(!plaser->ttl--){
			//newsmoke(global,plaser->x+(PLASER_WIDTH/2.0f),plaser->y+(PLASER_HEIGHT/2.0f),SMOKE_LASER_FIRE);
			plaser=deleteplaser(global,plaser,prevplaser);
			continue;
		}
		prevplaser=plaser;
		plaser=plaser->next;
	}
	
	for(struct particle *particle=global->particlelist,*prevparticle=NULL;particle!=NULL;){
		const float PARTICLE_DECAY=0.008f;
		particle->x+=particle->xup;
		particle->y+=particle->yup;
		particle->x+=(PARTICLE_DECAY/2.0f);
		particle->y+=(PARTICLE_DECAY/2.0f);
		particle->w-=PARTICLE_DECAY;
		particle->h-=PARTICLE_DECAY/(PARTICLE_WIDTH/PARTICLE_HEIGHT);
		if(particle->w<0.0f){
			particle=deleteparticle(global,particle,prevparticle);
			continue;
		}
		prevparticle=particle;
		particle=particle->next;
	}
	
	for(struct debris *debris=global->debrislist,*prevdebris=NULL;debris!=NULL;){
		debris->x+=debris->xup;
		debris->y+=debris->yup;
		debris->alpha-=0.05f;
		if(debris->alpha<0.0f){
			debris=deletedebris(global,debris,prevdebris);
			continue;
		}
		prevdebris=debris;
		debris=debris->next;
	}
	
	for(struct health *health=global->healthlist,*prevhealth=NULL;health!=NULL;){
		health->rot+=health->rotup;
		if(collide(&global->player,health)){
			health=deletehealth(global,health,prevhealth);
			global->player.health+=randomint(11,19);
			if(global->player.health>100)global->player.health=100;
			continue;
		}
		if(!health->ttl--){
			health=deletehealth(global,health,prevhealth);
			continue;
		}
		prevhealth=health;
		health=health->next;
	}
	
	for(struct smoke *smoke=global->smokelist,*prevsmoke=NULL;smoke!=NULL;){
		const float SMOKE_SPREAD=0.01f,SMOKE_FADE=0.01f;
		smoke->w+=SMOKE_SPREAD;
		smoke->h=smoke->w;
		smoke->x-=SMOKE_SPREAD/2.0f;
		smoke->y-=SMOKE_SPREAD/2.0f;
		smoke->rot+=smoke->rotup;
		smoke->alpha-=SMOKE_FADE;
		if(smoke->alpha<0.0f){
			smoke=deletesmoke(global,smoke,prevsmoke);
			continue;
		}
		prevsmoke=smoke;
		smoke=smoke->next;
	}
	
	if(global->back){
		global->back=false;
		if(!menu_pause(global))return false;
	}
	
	return true;
}
void render(struct global *global){
	glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
	if(global->player.x+PLAYER_WIDTH>global->world.right-9.0f||global->player.x<global->world.left+9.0f||
	global->player.y+PLAYER_HEIGHT>global->world.bottom-4.5f||global->player.y<global->world.top+4.5f){
		glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_THEVOID].object);
		uidraw(global,&(struct generic){TID_THEVOID,global->rect.left,global->rect.top,16.0f,9.0f,0.0f});
	}
	
	glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
	glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_BACKGROUND].object);
	draw(global,&(struct generic){TID_BACKGROUND,global->world.left,global->world.top,global->world.right,global->world.bottom,0.0f});
	draw(global,&(struct generic){TID_BACKGROUND,0.0f,global->world.top,global->world.right,global->world.bottom,0.0f});
	draw(global,&(struct generic){TID_BACKGROUND,global->world.left,0.0f,global->world.right,global->world.bottom,0.0f});
	draw(global,&(struct generic){TID_BACKGROUND,0.0f,0.0f,global->world.right,global->world.bottom,0.0f});
	
	glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_SMOKE].object);
	for(struct smoke *smoke=global->smokelist;smoke!=NULL;smoke=smoke->next){
		if(smoke->type==SMOKE_ENEMY_EXHAUST||smoke->type==SMOKE_ENEMY_DESTROY)glUniform4f(global->uniform.rgba,1.0f,0.0f,0.0f,smoke->alpha);
		else glUniform4f(global->uniform.rgba,0.0f,1.0f,0.0f,smoke->alpha);
		draw(global,smoke);
	}
	glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
	
	for(struct spawner *spawner=global->spawnerlist;spawner!=NULL;spawner=spawner->next){
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[spawner->texture].object);
		draw(global,spawner);
		if((spawner->health<100&&spawner->health>0&&onein(spawner->health/2))||(spawner->health<1&&onein(10))){
			glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_STUN].object);
			glUniform4f(global->uniform.rgba,0.0f,1.0f,0.0f,1.0f);
			draw(global,&(struct generic){TID_STUN,spawner->x,spawner->y,spawner->w,spawner->h,randomint(0,360)*(PI/180.0f)});
			glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_SPAWNER].object);
			glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
		}
	}
	
	int boundtexture=TID_DEBRIS;
	for(struct debris *debris=global->debrislist;debris!=NULL;debris=debris->next){
		if(global->assets.texture[debris->texture].object!=boundtexture){
			boundtexture=global->assets.texture[debris->texture].object;
			glBindTexture(GL_TEXTURE_2D,boundtexture);
		}
		glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,debris->alpha);
		draw(global,debris);
	}
	glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
	
	glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_HEALTH].object);
	for(struct health *health=global->healthlist;health!=NULL;health=health->next)
		draw(global,health);
	
	glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_ENEMY].object);
	for(struct enemy *enemy=global->enemylist;enemy!=NULL;enemy=enemy->next){
		draw(global,enemy);
		if(enemy->stun&&onein(5)){
			glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_STUN].object);
			glUniform4f(global->uniform.rgba,0.0f,1.0f,0.0f,1.0f);
			draw(global,&(struct generic){TID_STUN,enemy->x,enemy->y,ENEMY_WIDTH,ENEMY_HEIGHT,randomint(0,360)*(PI/180.0f)});
			glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_ENEMY].object);
			glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
		}
	}
	
	glUniform4f(global->uniform.rgba,1.0f,0.0f,0.0f,1.0f);
	glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_PLASER].object);
	for(struct elaser *elaser=global->elaserlist;elaser!=NULL;elaser=elaser->next)
		draw(global,elaser);
	
	glUniform4f(global->uniform.rgba,0.0f,1.0f,0.0f,1.0f);
	//glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_PLASER].object);
	for(struct plaser *plaser=global->plaserlist;plaser!=NULL;plaser=plaser->next)
		draw(global,plaser);
	glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
	
	glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_PLAYER].object);
	draw(global,&global->player);
	if(global->player.stun&&onein(5)){
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_STUN].object);
		glUniform4f(global->uniform.rgba,1.0f,0.0f,0.0f,1.0f);
		draw(global,&(struct generic){TID_STUN,global->player.x,global->player.y,PLAYER_WIDTH,PLAYER_HEIGHT,randomint(0,360)*(PI/180.0f)});
	}
	
	glUniform4f(global->uniform.rgba,0.0f,1.0f,0.0f,1.0);
	glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_PARTICLE].object);
	for(struct particle *particle=global->particlelist;particle!=NULL;particle=particle->next)
		draw(global,particle);
	glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
	
	glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_JOYSTICK_BASE].object);
	uidraw(global,&(struct button){TID_JOYSTICK_BASE,JOYSTICK_BASE_X,JOYSTICK_BASE_Y,JOYSTICK_BASE_SIZE,JOYSTICK_BASE_SIZE,0.0f});
	glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_JOYSTICK_TOP].object);
	uidraw(global,&global->joystick_top);
	if(global->firebutton.activepointer!=-1)glUniform4f(global->uniform.rgba,0.6f,0.6f,0.6f,1.0f);
	glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_FIREBUTTON].object);
	uidraw(global,&global->firebutton);
	
	const float healthborder=0.05f;
	glUniform4f(global->uniform.rgba,0.0f,0.3f,0.0f,1.0f);
	glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_WHITE].object);
	uidraw(global,&(struct generic){TID_WHITE,-4.5f,global->rect.top+0.3f,12.0f,0.5f,0.0f});
	glUniform4f(global->uniform.rgba,0.0f,0.0f,0.0f,1.0f);
	uidraw(global,&(struct generic){TID_WHITE,-4.5f+healthborder,global->rect.top+0.3f+healthborder,12.0f-healthborder*2.0f,0.5-healthborder*2.0f,0.0f});
	glUniform4f(global->uniform.rgba,0.0f,0.5f,0.0f,1.0f);
	uidraw(global,&(struct generic){TID_WHITE,-4.5f+healthborder,global->rect.top+0.3f+healthborder,(12.0f-healthborder*2.0f)*(global->player.displayhealth/100.0f),0.5-healthborder*2.0f,0.0f});
	glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_COMPASS].object);
	glUniform4f(global->uniform.rgba,0.4f,0.4f,0.4f,1.0f);
	uidraw(global,&(struct generic){TID_COMPASS,global->rect.left+0.1f,global->rect.top+0.2f,1.0f,1.0f,0.0f});
	glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_NEEDLE].object);
	glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
	for(struct spawner *spawner=global->spawnerlist;spawner!=NULL;spawner=spawner->next){
		float angle=-atan2f((global->player.y+(PLAYER_HEIGHT/2.0f))-(spawner->y+(SPAWNER_HEIGHT/2.0f)),(global->player.x+(PLAYER_WIDTH/2.0f))-(spawner->x+(SPAWNER_WIDTH/2.0f)));
		uidraw(global,&(struct generic){TID_NEEDLE,global->rect.left+0.1f,global->rect.top+0.7f,1.0f,0.05f,angle});
	}
	glUniform1f(global->uniform.rot,0.0f);
	glBindTexture(GL_TEXTURE_2D,global->font2->atlas);
	char hudtext[20];
	int spawnercount=0;
	for(struct spawner *spawner=global->spawnerlist;spawner!=NULL;spawner=spawner->next)spawnercount++;
	sprintf(hudtext,"%d/%d",SPAWNER_COUNT-spawnercount,SPAWNER_COUNT);
	drawtextcentered(global->font2,global->rect.left+2.2f,global->rect.top+0.2f,hudtext);
	
	if(global->messagetimer){
		glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,global->messagetimer/30.0f);
		glBindTexture(GL_TEXTURE_2D,global->font2->atlas);
		drawtextcentered(global->font2,0.0f,-2.0f,global->messagetext);
		global->messagetimer--;
	}
}
void init(struct global *global){
	global->pointer[0].active=false;
	global->pointer[1].active=false;
	global->pointer[0].x=global->rect.left;
	global->pointer[0].y=global->rect.top;
	//global->click=false;
	global->showmenu=true;
	global->back=false;
	global->titleseq=true;
	global->rect.right=8.0f;
	global->rect.bottom=4.5f;
	global->rect.left=-global->rect.right;
	global->rect.top=-global->rect.bottom;
	global->world.right=50.0f;
	global->world.bottom=50.0f;
	global->world.left=-global->world.right;
	global->world.top=-global->world.bottom;
	global->joystick_top=(struct button){TID_JOYSTICK_TOP,global->rect.left+1.0f,global->rect.bottom-JOYSTICK_BASE_SIZE-1.0f,JOYSTICK_BASE_SIZE,JOYSTICK_BASE_SIZE,0.0f,-1};
	global->firebutton=(struct button){TID_FIREBUTTON,global->rect.right-2.4f-JOYSTICK_BASE_SIZE,global->rect.bottom-JOYSTICK_BASE_SIZE-2.4f,JOYSTICK_BASE_SIZE*2.0f,JOYSTICK_BASE_SIZE*2.0f,0.0f,-1};
	global->player.texture=TID_PLAYER;
	global->player.w=PLAYER_WIDTH;
	global->player.h=PLAYER_HEIGHT;
	global->spawnerlist=NULL;
	global->enemylist=NULL;
	global->particlelist=NULL;
	global->healthlist=NULL;
	global->debrislist=NULL;
	global->plaserlist=NULL;
	global->elaserlist=NULL;
	global->smokelist=NULL;
}
void reset(struct global *global,int set_anew){
	for(struct spawner *spawner=global->spawnerlist;spawner!=NULL;spawner=deletespawner(global,spawner,NULL));
	global->spawnerlist=NULL;
	for(struct elaser *elaser=global->elaserlist;elaser!=NULL;elaser=deleteelaser(global,elaser,NULL));
	global->elaserlist=NULL;
	for(struct plaser *plaser=global->plaserlist;plaser!=NULL;plaser=deleteplaser(global,plaser,NULL));
	global->plaserlist=NULL;
	for(struct enemy *enemy=global->enemylist;enemy!=NULL;enemy=deleteenemy(global,enemy,NULL));
	global->enemylist=NULL;
	for(struct particle *particle=global->particlelist;particle!=NULL;particle=deleteparticle(global,particle,NULL));
	global->particlelist=NULL;
	for(struct health *health=global->healthlist;health!=NULL;health=deletehealth(global,health,NULL));
	global->healthlist=NULL;
	for(struct debris *debris=global->debrislist;debris!=NULL;debris=deletedebris(global,debris,NULL));
	global->debrislist=NULL;
	for(struct smoke *smoke=global->smokelist;smoke!=NULL;smoke=deletesmoke(global,smoke,NULL));
	global->smokelist=NULL;
	
	global->player.x=0.0f-(PLAYER_WIDTH/2.0f);
	global->player.y=0.0f-(PLAYER_HEIGHT/2.0f);
	global->player.xup=0.0f;
	global->player.yup=0.0f;
	global->player.rot=0.0f;
	global->player.xup=0.0f;
	global->player.yup=0.0f;
	global->player.reload=0;
	global->player.stun=0;
	global->player.displayhealth=75.0f;
	global->player.health=100;
	global->messagetimer=0;
	global->messagetext=NULL;
	global->search=false;
	if(set_anew){
		for(int i=0;i<SPAWNER_COUNT;i++){
			newspawner(global);
			for(int j=0;j<SPAWNER_MAX_GUARD_COUNT;j++){
				global->spawnerlist->guardcount++;
				newenemy(global,global->spawnerlist);
			}
		}
	}
}