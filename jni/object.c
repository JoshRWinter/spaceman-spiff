#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <android_native_app_glue.h>
#include <math.h>
#include "glesutil.h"
#include "defs.h"
void newspawner(struct global *global){
	float xpos,ypos;
	int retry;
	do{
		retry=false;
		xpos=randomint(global->world.left*20.0f,(global->world.right-SPAWNER_WIDTH)*20.0f)/20.0f;
		ypos=randomint(global->world.top*20.0f,(global->world.bottom-SPAWNER_HEIGHT)*20.0f)/20.0f;
		for(struct spawner *spawner=global->spawnerlist;spawner!=NULL;spawner=spawner->next)
			if(dist(xpos,ypos,spawner->x,spawner->y)<15.0f)retry=true;
	}while(retry);
	struct spawner *spawner=malloc(sizeof(struct spawner));
	spawner->texture=TID_SPAWNER;
	spawner->w=SPAWNER_WIDTH;
	spawner->h=SPAWNER_HEIGHT;
	//#define world rect
	spawner->x=xpos;
	spawner->y=ypos;
	//#undef world
	spawner->alert=false;
	spawner->guardcount=0;
	spawner->health=100;
	spawner->xup=0.0f;
	spawner->yup=0.0f;
	spawner->carrier=NULL;
	spawner->rot=randomint(0,360)*(PI/180.0f);
	spawner->rotup=(randomint(12,20)/100.0)*(randomint(0,1)?1.0f:-1.0f);
	spawner->next=global->spawnerlist;
	global->spawnerlist=spawner;
}
struct spawner *deletespawner(struct global *global,struct spawner *spawner,struct spawner *prevspawner){
	if(prevspawner!=NULL)prevspawner->next=spawner->next;
	else global->spawnerlist=spawner->next;
	void *temp=spawner->next;
	free(spawner);
	return temp;
}

void newenemy(struct global *global,struct spawner *spawner){
	//return;
	struct enemy *enemy=malloc(sizeof(struct enemy));
	enemy->texture=TID_ENEMY;
	enemy->w=ENEMY_WIDTH;
	enemy->h=ENEMY_HEIGHT;
	enemy->x=spawner->x+(SPAWNER_WIDTH/2.0f)-(ENEMY_WIDTH/2.0f);
	enemy->y=spawner->y+(SPAWNER_HEIGHT/2.0f)-(ENEMY_HEIGHT/2.0f);
	enemy->xup=0.0f;
	enemy->yup=0.0f;
	enemy->rot=randomint(0,360)*(PI/180.0f);
	enemy->mode=ENEMY_MODE_IDLE;
	enemy->health=100;
	enemy->stun=0;
	enemy->reload=0;
	enemy->spawner=spawner;
	enemy->next=global->enemylist;
	global->enemylist=enemy;
}
struct enemy *deleteenemy(struct global *global,struct enemy *enemy,struct enemy *prevenemy){
	if(prevenemy!=NULL)prevenemy->next=enemy->next;
	else global->enemylist=enemy->next;
	void *temp=enemy->next;
	free(enemy);
	return temp;
}

void newdebris(struct global *global,float x,float y,int type){
	struct debris *debris=malloc(sizeof(struct debris));
	float speed;
	debris->texture=TID_DEBRIS+type;
	switch(type){
		case DEBRIS_ENEMY_WING:
			debris->w=1.0f;
			debris->h=0.366f;
			speed=randomint(25,50);
			break;
		case DEBRIS_ENEMY_FUSELAGE:
			debris->w=0.7f;
			debris->h=0.366f;
			speed=randomint(300,350);
			break;
		case DEBRIS_SPAWNER_WING:
			debris->w=1.533f;
			debris->h=1.833f;
			speed=7;
			break;
	}
	debris->rot=randomint(0,360)*(PI/180.0f);
	debris->x=x-(debris->w/2.0f);
	debris->y=y-(debris->h/2.0f);
	debris->xup=cos(debris->rot)/speed;
	debris->yup=sin(debris->rot)/speed;
	debris->alpha=5.0f;
	debris->rotup=0.0f;
	debris->next=global->debrislist;
	global->debrislist=debris;
	sortdebris(&global->debrislist);
}
struct debris *deletedebris(struct global *global,struct debris *debris,struct debris *prevdebris){
	if(prevdebris!=NULL)prevdebris->next=debris->next;
	else global->debrislist=debris->next;
	void *temp=debris->next;
	free(debris);
	return temp;
}

void newparticles(struct global *global,float x,float y,float angle,int count){
	for(int i=0;i<count;i++){
		struct particle *particle=malloc(sizeof(struct particle));
		particle->texture=TID_PARTICLE;
		particle->w=PARTICLE_WIDTH;
		particle->h=PARTICLE_HEIGHT;
		particle->x=x-(PARTICLE_WIDTH/2.0f);
		particle->y=y-(PARTICLE_HEIGHT/2.0f);
		particle->rot=angle+(randomint(-10,10)/40.0f);
		float speed=randomint(10,60)/10.0f;
		particle->xup=-cos(-particle->rot)/speed;
		particle->yup=-sin(-particle->rot)/speed;
		particle->next=global->particlelist;
		global->particlelist=particle;
	}
}
struct particle *deleteparticle(struct global *global,struct particle *particle,struct particle *prevparticle){
	if(prevparticle!=NULL)prevparticle->next=particle->next;
	else global->particlelist=particle->next;
	void *temp=particle->next;
	free(particle);
	return temp;
}

void newhealth(struct global *global,float x,float y){
	if(x<global->world.left||y<global->world.top||x+HEALTH_WIDTH>global->world.right||y+HEALTH_HEIGHT>global->world.bottom)return;
	struct health *health=malloc(sizeof(struct health));
	health->texture=TID_HEALTH;
	health->w=HEALTH_WIDTH;
	health->h=HEALTH_HEIGHT;
	health->x=x-(HEALTH_WIDTH/2.0f);
	health->y=y-(HEALTH_HEIGHT/2.0f);
	health->rot=randomint(0,360)/(PI/180.0f);
	health->rotup=randomint(-10,10)/100.0f;
	health->ttl=300;
	health->next=global->healthlist;
	global->healthlist=health;
}
struct health *deletehealth(struct global *global,struct health *health,struct health *prevhealth){
	if(prevhealth!=NULL)prevhealth->next=health->next;
	else global->healthlist=health->next;
	void *temp=health->next;
	free(health);
	return temp;
}

void newsmoke(struct global *global,float x,float y,int type){
	struct smoke *smoke=malloc(sizeof(struct smoke));
	smoke->texture=TID_SMOKE;
	switch(type){
		case SMOKE_ENEMY_EXHAUST:
		case SMOKE_EXHAUST:
			smoke->w=randomint(70,100)/100.0f;
			smoke->h=smoke->w;
			smoke->alpha=0.18f;
			break;
		case SMOKE_LASER_FIRE:
			smoke->w=0.4f;
			smoke->h=smoke->w;
			smoke->alpha=0.4f;
			break;
		case SMOKE_ENEMY_DESTROY:
			smoke->w=1.5f;
			smoke->h=smoke->w;
			smoke->alpha=0.4f;
			break;
	}
	smoke->type=type;
	smoke->rotup=0.0f*(randomint(0,1)?1.0f:-1.0f);
	smoke->x=x-(smoke->w/2.0f);
	smoke->y=y-(smoke->h/2.0f);
	smoke->rot=randomint(0,360)*(PI/180.0f);
	smoke->next=global->smokelist;
	global->smokelist=smoke;
}
struct smoke *deletesmoke(struct global *global,struct smoke *smoke,struct smoke *prevsmoke){
	if(prevsmoke!=NULL)prevsmoke->next=smoke->next;
	else global->smokelist=smoke->next;
	void *temp=smoke->next;
	free(smoke);
	return temp;
}

void newelaser(struct global *global,struct enemy *enemy){
	struct elaser *elaser=malloc(sizeof(struct elaser));
	elaser->texture=TID_PLASER;
	elaser->w=ELASER_WIDTH;
	elaser->h=ELASER_HEIGHT;
	elaser->x=enemy->x+(ENEMY_WIDTH/2.0f)-(ELASER_WIDTH/2.0f);
	elaser->y=enemy->y+(ENEMY_HEIGHT/2.0f)-(ELASER_HEIGHT/2.0f);
	elaser->rot=-atan2f((elaser->y+(ELASER_HEIGHT/2.0f))-(global->player.y+(PLAYER_HEIGHT/2.0f)),
	(elaser->x+(ELASER_WIDTH/2.0f))-(global->player.x+(PLAYER_WIDTH/2.0f)));
	const float speed=10.0f;
	elaser->xup=-cosf(-elaser->rot)/speed;
	elaser->yup=-sinf(-elaser->rot)/speed;
	elaser->ttl=ELASER_TTL;
	elaser->next=global->elaserlist;
	global->elaserlist=elaser;
}
struct elaser *deleteelaser(struct global *global,struct elaser *elaser,struct elaser *prevelaser){
	if(prevelaser!=NULL)prevelaser->next=elaser->next;
	else global->elaserlist=elaser->next;
	void *temp=elaser->next;
	free(elaser);
	return temp;
}

void newplaser(struct global *global){
	struct plaser *plaser=malloc(sizeof(struct plaser));
	plaser->texture=TID_PLASER;
	plaser->w=PLASER_WIDTH;
	plaser->h=PLASER_HEIGHT;
	plaser->x=global->player.x+(PLAYER_WIDTH/2.0f)-(PLASER_WIDTH/2.0f);
	plaser->y=global->player.y+(PLAYER_HEIGHT/2.0f)-(PLASER_HEIGHT/2.0f);
	plaser->rot=global->player.rot+randomint(-10.0f,10.0f)/200.0f;
	plaser->xup=-cos(-plaser->rot)/8.5f;
	plaser->yup=-sin(-plaser->rot)/8.5f;
	plaser->next=global->plaserlist;
	global->plaserlist=plaser;
	plaser->ttl=PLASER_TTL;
	const float offset=3.0f;
	newsmoke(global,plaser->x+(PLASER_WIDTH/2.0f)+plaser->xup*offset+global->player.xup*offset,
	plaser->y+(PLASER_HEIGHT/2.0f)+plaser->yup*offset+global->player.yup*offset,SMOKE_LASER_FIRE);
}
struct plaser *deleteplaser(struct global *global,struct plaser *plaser,struct plaser *prevplaser){
	if(prevplaser!=NULL)prevplaser->next=plaser->next;
	else global->plaserlist=plaser->next;
	void *temp=plaser->next;
	free(plaser);
	return temp;
}
int enemypoint(struct enemy *enemy){
	if(enemy->rot==enemy->rottarget)return true;
	const float turnspeed=enemy->spawner->alert?0.09:0.04f;
	while(enemy->rottarget<0.0f)enemy->rottarget+=PI2;
	while(enemy->rottarget>PI2)enemy->rottarget-=PI2;
	while(enemy->rot<0.0f)enemy->rot+=PI2;
	while(enemy->rot>PI2)enemy->rot-=PI2;
	if(enemy->rot>enemy->rottarget){
		if(enemy->rot-enemy->rottarget>PI)enemy->rot+=turnspeed;
		else{
			enemy->rot-=turnspeed;
			if(enemy->rot<enemy->rottarget)enemy->rot=enemy->rottarget;
		}
	}
	else{
		if(enemy->rottarget-enemy->rot>PI)enemy->rot-=turnspeed;
		else{
			enemy->rot+=turnspeed;
			if(enemy->rot>enemy->rottarget)enemy->rot=enemy->rottarget;
		}
	}
	return false;
}
void sortdebris(struct debris **debrislist){
	int swaps;
	do{
		swaps=0;
		for(struct debris *debris=*debrislist,*prevdebris=NULL;debris!=NULL;){
			struct debris *nextdebris=debris->next;
			if(nextdebris){
				if(debris->texture>nextdebris->texture){
					if(prevdebris!=NULL)prevdebris->next=nextdebris;
					else *debrislist=nextdebris;
					debris->next=nextdebris->next;
					nextdebris->next=debris;
					swaps++;
					prevdebris=nextdebris;
					continue;
				}
			}
			prevdebris=debris;
			debris=nextdebris;
		}
	}while(swaps!=0);
}
void draw(struct global *global,void *vtarget){
	struct generic *target=vtarget;
	glUniform4fv(global->uniform.texcoords,1,global->assets.texture[target->texture].pinch);
	glUniform2f(global->uniform.vector,reposx(target->x),reposy(target->y));
	glUniform2f(global->uniform.size,target->w,target->h);
	glUniform1f(global->uniform.rot,target->rot);
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);
}
void uidraw(struct global *global,void *vtarget){
	struct generic *target=vtarget;
	glUniform4fv(global->uniform.texcoords,1,global->uiassets.texture[target->texture].pinch);
	glUniform2f(global->uniform.vector,target->x,target->y);
	glUniform2f(global->uniform.size,target->w,target->h);
	glUniform1f(global->uniform.rot,target->rot);
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);
}
int collide(void *va,void *vb){
	struct generic *a=va,*b=vb;
	return a->x+a->w>b->x&&a->x<b->x+b->w&&a->y+a->h>b->y&&a->y<b->y+b->h;
}
int touching(struct global *global,void *vtarget,int *id){
	struct generic *target=vtarget;
	if(global->pointer[0].active&&global->pointer[0].x>target->x&&global->pointer[0].x<target->x+target->w&&global->pointer[0].y>target->y&&global->pointer[0].y<target->y+target->h){
		if(id)*id=0;
		return true;
	}
	if(global->pointer[1].active&&global->pointer[1].x>target->x&&global->pointer[1].x<target->x+target->w&&global->pointer[1].y>target->y&&global->pointer[1].y<target->y+target->h){
		if(id)*id=1;
		return true;
	}
	return false;
}