#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <android_native_app_glue.h>
#include "glesutil.h"
#include "defs.h"
int menu_title(struct global *global){
	float alpha=0.0f;
	int fade=false;
	while(process(global->app)){
		glClear(GL_COLOR_BUFFER_BIT);
		glBindTexture(GL_TEXTURE_2D,global->font2->atlas);
		char *title="Josh Winter";
		glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,alpha);
		drawtextcentered(global->font2,0.0f,-global->font2->fontsize/2.0f,title);
		alpha+=0.02f*(fade?-1.0f:1.0f);
		if(alpha>2.0f)fade=true;
		if(alpha<0.0f)break;
		if(global->pointer[0].active)break;
		if(global->back)ANativeActivity_finish(global->app->activity);
		eglSwapBuffers(global->display,global->surface);
	}
	return global->running;
}
int menu_main(struct global *global){
	if(global->titleseq){
		global->titleseq=false;
		if(!menu_title(global))return false;
	}
	struct{float x,y;int active;const char *name;}item[]={
		{-5.05f,-1.0f,false,"Play"},
		{0.05f,-1.0f,false,"Tutorial"},
		{-5.05f,0.6f,false,"Aboot"},
		{0.05f,0.6f,false,"Quit"}
	};
	int active=global->pointer[0].active;
	while(process(global->app)){
		glClear(GL_COLOR_BUFFER_BIT);
		glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
		glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_TITLE].object);
		uidraw(global,&(struct generic){TID_TITLE,global->rect.left,global->rect.top,global->rect.right*2.0f,global->rect.bottom*2.0f,0.0f});
		glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_BUTTON].object);
		int leave=false;
		for(int i=0;i<4;i++){
			if(global->pointer[0].x>item[i].x&&global->pointer[0].y>item[i].y&&global->pointer[0].x<item[i].x+5.0f&&global->pointer[0].y<item[i].y+1.5f){
				if(global->pointer[0].active&&!active)item[i].active=true;
				if(item[i].active){
					glUniform4f(global->uniform.rgba,0.6f,0.6f,0.6f,1.0f);
					if(!global->pointer[0].active){
						item[i].active=false;
						if(i==0)leave=true;
						else if(i==1){
							if(!menu_tutorial(global))return false;
							break;
						}
						else if(i==2){
							if(!menu_message(global,ABOUT_TEXT))return false;
							break;
						}
						else if(i==3)ANativeActivity_finish(global->app->activity);
					}
				}
			}
			else{
				glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
				item[i].active=false;
			}
			uidraw(global,&(struct generic){TID_BUTTON,item[i].x,item[i].y,5.0f,1.5f,0.0f});
		}
		glBindTexture(GL_TEXTURE_2D,global->font2->atlas);
		glUniform4f(global->uniform.rgba,0.0f,0.0f,0.0f,1.0f);
		for(int i=0;i<4;i++){
			drawtextcentered(global->font2,item[i].x+2.5f,item[i].y+0.3f,item[i].name);
		}
		if(leave)break;
		if(global->back)ANativeActivity_finish(global->app->activity);
		active=global->pointer[0].active;
		eglSwapBuffers(global->display,global->surface);
	}
	return global->running;
}
int menu_tutorial(struct global *global){
	const char *info[]={
		"Welcome to Spaceman Spiff.\n\nThis tutorial will teach you how to play the game.",
		"- Controls -\n\nUse the joystick on the bottom left of the screen to navigate.\nUse the fire-button on the bottom right to fire.\nPress the 'back' button on your phone to reach the pause menu.",
		"- HUD -\n\nUse the compass at the top left to find your objective.\nUse the 'x/3' indicator to track your progress.\nThe green bar at the top represents your health.",
		"- Spawners -\n\nThree spawners are randomly placed throughout the world.\nShoot the spawner to destroy it, then collect the core (drive over it).\nDon't touch the spawner until you destroy it.",
		"- Enemies -\n\nSpawners spawn up to five guards.\nIf you destroy one, the spawner will eventually spawn another.\nThe enemies will attack you when provoked.\nUpon destruction of the spawner, "
			"the enemies will rush to the core.\nIf you collect the core before they do, it will disable them.",
		"- Health -\n\nEnemies may drop health capsules when killed,\ncollect them to replenish your health.",
		"- Endgame -\n\nThe game ends when either you die or you collect all 3 spawner cores.",
		"- The Gist -\n\nShoot the spinny things.\nDon't die.",
		"whatever it doesnt even matter youll probaly be terrible anyway\nyou should just uninstall this app honestly"
	};
	const int infocount=9;
	int infoindex=0;
	int active=global->pointer[0].active;
	struct{float x;int active;}item[]={
		{-7.0f,false},
		{2.0f,false}
	};
	while(process(global->app)){
		glClear(GL_COLOR_BUFFER_BIT);
		glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
		glBindTexture(GL_TEXTURE_2D,global->font1->atlas);
		drawtextcentered(global->font1,0.0f,global->rect.top+0.25f,info[infoindex]);
		char counter[5];
		sprintf(counter,"(%d/%d)",infoindex+1,infocount);
		drawtextcentered(global->font1,0.0f,global->rect.bottom-1.5f,counter);
		
		glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_BUTTON].object);
		int leave=false;
		for(int i=0;i<2;i++){
			if(global->pointer[0].x>item[i].x&&global->pointer[0].y>2.5f&&global->pointer[0].x<item[i].x+5.0f&&global->pointer[0].y<4.0f){
				if(global->pointer[0].active&&!active)item[i].active=true;
				if(item[i].active){
					glUniform4f(global->uniform.rgba,0.6f,0.6f,0.6f,1.0f);
					if(!global->pointer[0].active){
						item[i].active=false;
						if(i==0&&--infoindex<0)leave=true;
						else if(i==1&&++infoindex==infocount)leave=true;
					}
				}
			}
			else{
				glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
				item[i].active=false;
			}
			uidraw(global,&(struct generic){TID_BUTTON,item[i].x,2.5f,5.0f,1.5f,0.0f});
		}
		glBindTexture(GL_TEXTURE_2D,global->font1->atlas);
		glUniform4f(global->uniform.rgba,0.0f,0.0f,0.0f,1.0f);
		for(int i=0;i<2;i++){
			const char *name;
			if(i==0){
				if(infoindex>0)name="Prev";
				else name="Back";
			}
			else{
				if(infoindex<infocount-1)name="Next";
				else name="Return";
			}
			drawtextcentered(global->font1,item[i].x+2.5f,2.9f,name);
		}
		
		if(leave)break;
		if(global->back){
			global->back=false;
			break;
		}
		active=global->pointer[0].active;
		eglSwapBuffers(global->display,global->surface);
	}
	glClear(GL_COLOR_BUFFER_BIT);
	return global->running;
}
int menu_message(struct global *global,const char *text){
	int active=global->pointer[0].active,okactive=false;
	float w=textlen(global->font1,text)+1.0f;
	if(w<10.0f)w=10.0f;
	float h=textheight(global->font1,text)+1.0f;
	const float margin=0.0f;
	while(process(global->app)){
		glClear(GL_COLOR_BUFFER_BIT);
		//glUniform4f(global->uniform.rgba,0.2f,0.2f,0.2f,1.0f);
		glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_WHITE].object);
		//uidraw(global,&(struct generic){TID_WHITE,global->rect.left+margin,global->rect.top+margin,(global->rect.right*2.0f)-(margin*2.0f),6.0f+(border*2.0f),0.0f});
		glUniform4f(global->uniform.rgba,0.3f,0.3f,0.3f,1.0f);
		uidraw(global,&(struct generic){TID_WHITE,global->rect.left+margin,global->rect.top+margin,(global->rect.right*2.0f)-((margin)*2.0f),6.5f,0.0f});
		glBindTexture(GL_TEXTURE_2D,global->font1->atlas);
		glUniform4f(global->uniform.rgba,0.75f,0.75f,0.75f,1.0f);
		drawtextcentered(global->font1,0.0f,global->rect.top+0.7f,text);
		
		int leave=false;
		glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
		glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_BUTTON].object);
		if(global->pointer[0].x>-2.5f&&global->pointer[0].y>2.5f&&global->pointer[0].x<2.5f&&global->pointer[0].y<4.0f){
			if(global->pointer[0].active&&!active)okactive=true;
			if(okactive){
				glUniform4f(global->uniform.rgba,0.6f,0.6f,0.6f,1.0f);
				if(!global->pointer[0].active){
					okactive=false;
					leave=true;
				}
			}
		}
		else{
			glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
			okactive=false;
		}
		uidraw(global,&(struct generic){TID_BUTTON,-2.5f,2.5f,5.0f,1.5f,0.0f});
		glBindTexture(GL_TEXTURE_2D,global->font1->atlas);
		glUniform4f(global->uniform.rgba,0.0f,0.0f,0.0f,1.0f);
		drawtextcentered(global->font1,0.0f,3.0f,"OK");
		
		if(leave)break;
		if(global->back){
			global->back=false;
			break;
		}
		active=global->pointer[0].active;
		eglSwapBuffers(global->display,global->surface);
	}
	glClear(GL_COLOR_BUFFER_BIT);
	return global->running;
}
int menu_pause(struct global *global){
	struct{float x,y;int active;const char *name;}item[]={
		{-5.05,-1.0f,false,"Resume"},
		{0.05f,-1.0f,false,"Menu"},
		{-5.05f,0.6f,false,"Reset"},
		{0.05f,0.6f,false,"Quit"}
	};
	int active=global->pointer[0].active;
	while(process(global->app)){
		glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
		glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_PAUSED].object);
		uidraw(global,&(struct generic){TID_PAUSED,global->rect.left,global->rect.top,global->rect.right*2.0f,global->rect.bottom*2.0f,0.0f});
		int leave=false;
		glBindTexture(GL_TEXTURE_2D,global->uiassets.texture[TID_BUTTON].object);
		for(int i=0;i<4;i++){
			if(global->pointer[0].x>item[i].x&&global->pointer[0].y>item[i].y&&global->pointer[0].x<item[i].x+5.0f&&global->pointer[0].y<item[i].y+1.5f){
				if(global->pointer[0].active&&!active)item[i].active=true;
				if(item[i].active){
					glUniform4f(global->uniform.rgba,0.6f,0.6f,0.6f,1.0f);
					if(!global->pointer[0].active){
						item[i].active=false;
						if(i==0)leave=true;
						else if(i==1){
							leave=true;
							global->showmenu=true;
						}
						else if(i==2){
							reset(global,true);
							leave=true;
						}
						else if(i==3)ANativeActivity_finish(global->app->activity);
					}
				}
			}
			else{
				item[i].active=false;
				glUniform4f(global->uniform.rgba,1.0f,1.0f,1.0f,1.0f);
			}
			uidraw(global,&(struct generic){TID_BUTTON,item[i].x,item[i].y,5.0f,1.5f,0.0f});
		}
		glUniform4f(global->uniform.rgba,0.0f,0.0f,0.0f,1.0f);
		glBindTexture(GL_TEXTURE_2D,global->font2->atlas);
		for(int i=0;i<4;i++){
			drawtextcentered(global->font2,item[i].x+2.5f,item[i].y+0.3f,item[i].name);
		}
		eglSwapBuffers(global->display,global->surface);
		active=global->pointer[0].active;
		if(leave)break;
		if(global->back){
			global->back=false;
			break;
		}
	}
	glClear(GL_COLOR_BUFFER_BIT);
	return global->running;
}
int menu_gameover(struct global *global){
	char *diemessage=malloc(250);
	if(global->spawnerlist)sprintf(diemessage,"%s\n\nyou collected %d pixie fart-coins",DIE_MESSAGE,randomint(220,490));
	else sprintf(diemessage,"You dun it!!\nyou have done okey job mate you have earned\nyourself an ice cream go buy an ice cream\nand enjoy it cause you saved mamkind\nice cream");
	if(!menu_message(global,diemessage)){
		free(diemessage);
		return false;
	}
	free(diemessage);
	reset(global,true);
	global->showmenu=true;
	glClear(GL_COLOR_BUFFER_BIT);
	return global->running;
}