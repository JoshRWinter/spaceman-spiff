#define TID_BACKGROUND 0
#define TID_SPAWNER 1
#define TID_SPAWNERCORE 2
#define TID_PLAYER 3
#define TID_ENEMY 4
#define TID_PLASER 5
#define TID_PARTICLE 6
#define TID_SMOKE 7
#define TID_STUN 8
#define TID_HEALTH 9
#define TID_DEBRIS 10

#define TID_TITLE 0
#define TID_PAUSED 1
#define TID_BUTTON 2
#define TID_THEVOID 3
#define TID_JOYSTICK_BASE 4
#define TID_JOYSTICK_TOP 5
#define TID_FIREBUTTON 6
#define TID_WHITE 7
#define TID_COMPASS 8
#define TID_NEEDLE 9

#define SID_MUSIC 0
#define SID_PLASER 1
#define SID_ELASER 2
#define SID_EXPLOSION 3
#define SID_BIGEXPLOSION 4

#define FILTER_ASSETS "1111111111000"
#define FONT_MAIN "Radiof.ttf"
#define FONT_DISPLAY "micross.ttf"
#define MESSAGE_SPAWNER "Spawner Collected!"
#define MESSAGE_TIMER 90
#define PATH "/data/data/com.joshwinter.spacemanspiff/files/"
#define PI 3.14159f
#define PI2 6.28318f
#define ALERT_RANGE 6.0f
#define ABOUT_TEXT "Spaceman Spiff created by Josh Winter in November of 2014.\nFonts used: "FONT_MAIN" and "FONT_DISPLAY".\nMusic: Perturbator - Miami Disco\nPlease ask for the gluten-free version of this game."
#define DIE_MESSAGE "ayy ur not suposed to have done that.\nthet means yuo died and now ur are in spaceship man heaven"

#define reposx(xp) (xp-global->player.x-(PLAYER_WIDTH/2.0f))
#define reposy(yp) (yp-global->player.y-(PLAYER_HEIGHT/2.0f))
#define dist(x1,y1,x2,y2) (sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)))
#define inrange(spawner) (fabs((global->player.x+(PLAYER_WIDTH/2.0f))-(spawner->x+(SPAWNER_WIDTH/2.0f)))<ALERT_RANGE&&\
	fabs((global->player.y+(PLAYER_HEIGHT/2.0f))-(spawner->y+(SPAWNER_HEIGHT/2.0f)))<ALERT_RANGE)

#define log logcat

struct generic{
	int texture;
	float x,y,w,h,rot;
};

#define JOYSTICK_BASE_SIZE 1.25f
#define JOYSTICK_BASE_X global->rect.left+1.6f
#define JOYSTICK_BASE_Y global->rect.bottom-JOYSTICK_BASE_SIZE-1.6f
#define JOYSTICK_DIST 1.3f
struct button{
	int texture;
	float x,y,w,h,rot;
	int activepointer;
};

#define PLAYER_WIDTH 1.0f
#define PLAYER_HEIGHT 1.0f
struct player{
	int texture;
	float x,y,w,h,rot,xup,yup,displayhealth;
	int reload,stun,health;
};

#define DEBRIS_ENEMY_WING 0
#define DEBRIS_ENEMY_FUSELAGE 1
#define DEBRIS_SPAWNER_WING 2
struct debris{
	int texture;
	float x,y,w,h,rot,xup,yup,rotup,alpha;
	struct debris *next;
};

#define ENEMY_WIDTH 1.0f
#define ENEMY_HEIGHT 1.0f
#define ENEMY_MODE_IDLE 0     // enemy is chillin'
#define ENEMY_MODE_RELOCATE 1 // enemy is meandering over to a waypoint
#define ENEMY_MODE_FIRE 2     // enemy is shooting at player
#define ENEMY_MODE_PROTECT 3  // enemy is rushing to the spawner to pick it up
#define ENEMY_MODE_DISABLE 4  // enemy is disabled after spawner is destroyed
#define ENEMY_SEEK_RANGE 8.0f
#define ENEMY_FORGET 300
#define ENEMY_STUN 45
#define ENEMY_RELOAD 45
struct enemy{
	int texture;
	float x,y,w,h,rot,xup,yup,xtarget,ytarget,rottarget;
	int mode,health,stun,reload;
	struct spawner *spawner;
	struct enemy *next;
};

#define SPAWNER_WIDTH 2.5f
#define SPAWNER_HEIGHT 2.5f
#define SPAWNER_COUNT 3
#define SPAWNER_MAX_GUARD_COUNT 5
#define SPAWNERCORE_WIDTH 1.566f
#define SPAWNERCORE_HEIGHT 1.0f
struct spawner{
	int texture;
	float x,y,w,h,rot,rotup,xup,yup;
	int alert,guardcount,health;
	struct enemy *carrier;
	struct spawner *next;
};

#define PLASER_WIDTH 0.9f
#define PLASER_HEIGHT 0.35f
#define PLASER_TTL 30
struct plaser{
	int texture;
	float x,y,w,h,rot,xup,yup;
	int ttl;
	struct plaser *next;
};

#define ELASER_WIDTH 0.9f
#define ELASER_HEIGHT 0.35f
#define ELASER_TTL 35
struct elaser{
	int texture;
	float x,y,w,h,rot,xup,yup;
	int ttl;
	struct elaser *next;
};

#define PARTICLE_WIDTH 0.6f
#define PARTICLE_HEIGHT 0.1f
struct particle{
	int texture;
	float x,y,w,h,rot,xup,yup;
	struct particle *next;
};

#define HEALTH_WIDTH 0.7f
#define HEALTH_HEIGHT 0.3f
struct health{
	int texture;
	float x,y,w,h,rot,rotup;
	int ttl;
	struct health *next;
};

#define SMOKE_EXHAUST 0
#define SMOKE_ENEMY_EXHAUST 1
#define SMOKE_LASER_FIRE 2
#define SMOKE_ENEMY_DESTROY 3
struct smoke{
	int texture;
	float x,y,w,h,rot,rotup,alpha;
	int type;
	struct smoke *next;
};

struct global{
	int running,showmenu,messagetimer,search,back,titleseq;
	float windoww,windowh;
	struct pack assets,uiassets;
	struct apack sassets;
	unsigned program,vbo,vao;
	const char *messagetext;
	ftfont *font1,*font2;
	EGLDisplay display;
	EGLSurface surface;
	EGLContext context;
	slesenv *soundengine;
	struct android_app *app;
	struct{float left,right,bottom,top;}rect,world;
	struct{int vector,size,texcoords,rgba,rot,projection;}uniform;
	struct{int active;float x,y;}pointer[2];
	struct button joystick_top,firebutton;
	struct spawner *spawnerlist;
	struct player player;
	struct debris *debrislist;
	struct enemy *enemylist;
	struct plaser *plaserlist;
	struct elaser *elaserlist;
	struct particle *particlelist;
	struct health *healthlist;
	struct smoke *smokelist;
};

void newdebris(struct global *global,float,float,int);
struct debris *deletedebris(struct global*,struct debris*,struct debris*);
void newspawner(struct global*);
struct spawner *deletespawner(struct global*,struct spawner*,struct spawner*);
void newsmoke(struct global*,float,float,int);
struct smoke *deletesmoke(struct global*,struct smoke*,struct smoke*);
void newparticles(struct global*,float,float,float,int);
struct particle *deleteparticle(struct global*,struct particle*,struct particle*);
void newhealth(struct global*,float,float);
struct health *deletehealth(struct global*,struct health*,struct health*);
void newenemy(struct global*,struct spawner*);
struct enemy *deleteenemy(struct global*,struct enemy*,struct enemy*);
void newelaser(struct global*,struct enemy*);
struct elaser *deleteelaser(struct global*,struct elaser*,struct elaser*);
void newplaser(struct global*);
struct plaser *deleteplaser(struct global*,struct plaser*,struct plaser*);

int menu_main(struct global*);
int menu_tutorial(struct global*);
int menu_message(struct global*,const char*);
int menu_pause(struct global*);
int menu_gameover(struct global*);

int core(struct global*);
void render(struct global*);
void init(struct global*);
void reset(struct global*,int);
int collide(void*,void*);
int touching(struct global*,void*,int*);
int enemypoint(struct enemy *enemy);
void sortdebris(struct debris**);
void draw(struct global*,void*);
void uidraw(struct global*,void*);

int process(struct android_app*);
int32_t inputproc(struct android_app*,AInputEvent*);
void cmdproc(struct android_app*,int32_t);
